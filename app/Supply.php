<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Supply extends Model
{
    public function requests()
    {
        return $this->morphMany('\App\Request','requestable');
    }

    use Sortable;

    protected $fillable = ['name', 'description', 'stock'];

    public $sortable = ['id', 'name', 'description', 'stock', 'created_at', 'updated_at'];
}
