<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $totalSuppliesRequests = \App\Request::whereHasMorph('requestable', ['App\Supply'])->count();
        $totalEquipmentRequests = \App\Request::whereHasMorph('requestable', ['App\Equipment'])->count();

        return view('dashboard', compact('totalSuppliesRequests', 'totalEquipmentRequests'));
    }
}
