<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Status;
use Illuminate\Database\Eloquent\Builder;
use Auth;

class ApprovalRequestController extends Controller
{
    public function index(Request $req)
    {
        $statusId = $req->status_id;
        $statuses = Status::all();

        $userId = Auth::user()->id;
        $pendingStatus = Status::where(['name' => 'Pending'])->first();

        $supplyRequests = $this->getSupplyRequests($req->status_id, $userId, $pendingStatus);
        $equipmentRequests = $this->getEquipmentRequests($req->status_id, $userId, $pendingStatus);

        return view('/approvalrequests', compact('supplyRequests', 'equipmentRequests', 'statuses', 'statusId'));
    }
    
    private function getEquipmentRequests($statusId, $userId, $pendingStatus)
    {
        $equipmentRequests = \App\Request::whereHasMorph(
            'requestable', 
            ['App\Equipment'],
            function(Builder $query) use($statusId) {
                if(isset($statusId) && $statusId > 0) {
                    $query->where("status_id", $statusId);
                }
            }
        )->get();

        // Filter out requests to be approved by the current user
        $equipmentRequests = $equipmentRequests->reject(function ($request) use($userId) {
            $requester = $request->requester;

            if ($requester->manager_id != $userId) {
                return true;
            }

            return false;
        });

        // Override the request status with the approval request status
        $equipmentRequests->each(function($request) use($pendingStatus, $userId) {
            $request->admin_status_id = $pendingStatus->id;
            $request->admin_status_name = $pendingStatus->name;

            $request->approvalRequests()->each(function($approvalRequest) use($request, $userId) {
                if ($approvalRequest->approver_id === $userId) {
                    $request->status_id = $approvalRequest->status_id;
                } else {
                    $request->admin_status_name = $approvalRequest->status->name;
                }
            });
        });

        return $equipmentRequests;
    }

    private function getSupplyRequests($statusId, $userId, $pendingStatus)
    {
        $supplyRequests = \App\Request::whereHasMorph(
            'requestable', 
            ['App\Supply'],
            function(Builder $query) use($statusId) {
                if(isset($statusId) && $statusId > 0) {
                    $query->where("status_id", $statusId);
                }
            }
        )->get();

        $supplyRequests = $supplyRequests->reject(function ($request) use($userId) {
            $requester = $request->requester;

            if ($requester->manager_id != $userId) {
                return true;
            }
            return false;
        });

        $supplyRequests->each(function($request) use($pendingStatus, $userId) {
            $request->admin_status_name = $pendingStatus->name;

            $request->approvalRequests->each(function($approvalRequest) use($request, $userId) {
                if ($approvalRequest->approver_id === $userId) {
                    $request->status_id = $approvalRequest->status_id;
                } else {
                    $request->admin_status_name = $approvalRequest->status->name;
                }
            });
        });

        return $supplyRequests;
    }
}
