<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class ProfileController extends Controller
{
    public function index()
    {
        return view('profile');
    }
    
    public function update(Request $request)
    {
        $rules = array(
            'name' => 'required',
            'email' => 'required|email'
        );

        $this->validate($request, $rules);

        $user = Auth::user();

        $user->name = $request->name;
        $user->email = $request->email;
        $user->save();

        return view('profile');
    }
}
