<?php

namespace App\Http\Controllers;

use App\ApprovalRequest;
use Illuminate\Http\Request;
use App\Supply;
use App\User;
use App\Status;
use Session;
use Auth;
use App\Request as RequestModel;

class SupplyRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $supplyRequests = RequestModel::whereHasMorph(
            'requestable',
            ['App\Supply']
        )->get();

        $supplyRequests = $supplyRequests->reject(function($request) {
            if ($request->status->name === 'Pending') {
                if ($request->approvalRequests->count() === 0) {
                    return true;
                } else {
                    $approvedStatusId = Status::where('name', 'Approved')->first()->id;

                    if($request->approvalRequests->where('status_id', $approvedStatusId)->count() === 0) {
                        return true;
                    }
                }
            } else if ($request->status->name === 'Disapproved') {
                if ($request->approvalRequests->count() > 0) {
                    $disapprovedStatusId = Status::where('name', 'Disapproved')->first()->id;

                    $managerId = $request->requester->manager->id;

                    if($request->approvalRequests->where('status_id', $disapprovedStatusId)->where('approver_id', $managerId)->count() > 0) {
                        return true;
                    }
                }
            }

            return false;
        });

        return view('/suppliesrequests', compact('supplyRequests'));
    }

    public function store(Request $request)
    {
        $rules = array(
            "user"     => "required",
            "quantity" => "required",
        );

        $this->validate($request, $rules);

        // Create a request
        Supply::find($request->supply)->requests()->create([
            "user_id" => $request->user,
            "quantity" => $request->quantity,
            "status_id" => 1
        ]);

        // Return a response.
        return response()->json([
            'message' => 'test'
        ]);
    }

    public function approveRequest($id)
    {
        $request = RequestModel::find($id);

        $supply = $request->requestable;

        $manager = $request->requester()->first()->manager()->first();

        if (is_null($manager)) {
            Session::flash("message", "Cannot approve request. Employee has no manager.");

            return redirect(url()->previous());
        } 
        
        $approverId = Auth::user()->id;
        $approvedStatusId = Status::where('name', 'Approved')->first()->id;

        $approval = $request->approvalRequests()->where([
            'approver_id' => $approverId
        ])->first();

        if($manager->id === $approverId) {
            // The approver of this request is the manager
            if (is_null($approval)) {
                // No approval yet from the manager. Create one.
                $request->approvalRequests()->create([
                    'status_id'   => $approvedStatusId,
                    'approver_id' => $approverId
                ]);
            } else {
                // Approval already exists, just update the current approval
                $approval->status_id = $approvedStatusId;
                $approval->save();
            }

            // Update the overall status of the request.
            $request->status_id = Status::where('name', 'Pending')->first()->id;; 
            $request->save();            
        } else {
            // The approver of this request is the admin staff.
            if ($request->quantity > $supply->stock) {
                Session::flash("message", "Cannot approve request. Quantity requested is more than the stock available.");
                return redirect(url()->previous());
            } else {
                $supply->stock = $supply->stock - $request->quantity;
            }
    
            $supply->save();

            if (is_null($approval)) {
                // No approval yet from the admin. Create one.
                $request->approvalRequests()->create([
                    'status_id'   => $approvedStatusId,
                    'approver_id' => $approverId
                ]);
            } else {
                // Approval already exists, just update the current approval
                $approval->status_id = $approvedStatusId;
                $approval->save();
            }
    
            // Update the overall status of the request.
            $request->status_id = $approvedStatusId; 
            $request->save();
        }

        return redirect(url()->previous());
    }
    
    public function disapproveRequest($id)
    {
        $request = RequestModel::find($id);

        $manager = $request->requester()->first()->manager()->first();

        if (is_null($manager)) {
            Session::flash("message", "Cannot disapprove request. Employee has no manager.");

            return redirect(url()->previous());
        } 
        
        $approverId = Auth::user()->id;
        $disapprovedStatusId = Status::where('name', 'Disapproved')->first()->id;

        $approval = $request->approvalRequests()->where([
            'approver_id' => $approverId
        ])->first();

        if($manager->id === $approverId) {
            // The disapprover of this request is the manager
            if (is_null($approval)) {
                // No approval yet from the manager. Create one.
                $request->approvalRequests()->create([
                    'status_id'   => $disapprovedStatusId,
                    'approver_id' => $approverId
                ]);
            } else {
                // Approval already exists, just update the current approval
                $approval->status_id = $disapprovedStatusId;
                $approval->save();
            }
        } else {
            // The disapprover of this request is the admin staff.
            if (is_null($approval)) {
                // No approval yet from the admin. Create one.
                $request->approvalRequests()->create([
                    'status_id'   => $disapprovedStatusId,
                    'approver_id' => $approverId
                ]);
            } else {
                // Approval already exists, just update the current approval
                $approval->status_id = $disapprovedStatusId;
                $approval->save();
            }
        }

        // Update the overall status of the request.
        $request->status_id = $disapprovedStatusId;
        $request->save();

        return redirect(url()->previous());
    }

    public function claimRequest($id)
    {
        $request = RequestModel::find($id);

        $claimedStatus = Status::where('name', 'Claimed')->first();
        $request->status_id = $claimedStatus->id; 
        $request->save();

        return redirect(url()->previous());
    }
}
