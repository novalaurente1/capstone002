<?php

namespace App\Http\Controllers;

use App\Supply;
use Illuminate\Http\Request;

class SupplyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $supplies = Supply::sortable()->paginate(5);
        return view('/supplies', compact('supplies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('/addsupply');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            "name" => "required",
            "description" => "required",
            "stock" => "required"
        );

        $this->validate($request, $rules);

        $new_supply = new Supply;
        $new_supply->name = $request->name;
        $new_supply->description = $request->description;
        $new_supply->stock = $request->stock;

        $new_supply->save();

        return redirect('/supplies');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Supply  $supply
     * @return \Illuminate\Http\Response
     */
    public function show(Supply $supply)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Supply  $supply
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('/editsupply');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Supply  $supply
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $supply = Supply::find($id);

        $rules = array(
            "name" => "required",
            "description" => "required",
            "stock" => "required"
        );

        $this->validate($request, $rules);

        $supply->name = $request->name;
        $supply->description = $request->description;
        $supply->stock = $request->stock;

        $supply->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Supply  $supply
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $supply = Supply::find($id);
        $supply->delete();

        return redirect('/supplies');
    }
}
