<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Supply;
use App\Equipment;

class RequestController extends Controller
{
    public function store(Request $request)
    {
        $rules = array(
            "user" => "required",
            "quantity" => "required",
            "request_type" => "required"
        );

        $this->validate($request, $rules);


        if ($request->request_type === 'supply') {
            // Create a request
            $supply = Supply::find($request->supply);
    
            $supply->requests()->create([
                "user_id" => $request->user,
                "quantity" => $request->quantity,
                "status_id" => 1
            ]);
        } else if ($request->request_type === 'equipment') {
            // Create a request
            $equipment = Equipment::find($request->equipment);
    
            $equipment->requests()->create([
                "user_id" => $request->user,
                "quantity" => $request->quantity,
                "status_id" => 1
            ]);
        }

        // Return a response.
        return response()->json([
            'message' => 'successful'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Supply  $supply
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requestData = \App\Request::find($id);

        $rules = array(
            "quantity" => "required",
        );

        $this->validate($request, $rules);

        $requestData->quantity = $request->quantity;
        $requestData->save();

        return response()->json([
            'message' => 'OK'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Supply  $supply
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $requestData = \App\Request::find($id);
        $requestData->delete();

        return redirect()->back();
    }
}
