<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Request as RequestModel;
use App\Status;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $totalRequests = RequestModel::where('user_id', Auth::user()->id)
            ->where('status_id', Status::where('name', 'Pending')->first()->id)
            ->count();

        return view('home', compact('totalRequests'));
    }
}
