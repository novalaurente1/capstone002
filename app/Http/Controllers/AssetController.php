<?php

namespace App\Http\Controllers;

use Auth;
use App\Supply;
use App\Equipment;
use App\Status;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;

class AssetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $req)
    {
        $supplies = Supply::all();
        $statusId = $req->status_id;

        if(isset($req->status_id) && $req->status_id>0)
        {
            $statuses = Status::all();
            $supplyRequests = \App\Request::whereHasMorph(
                'requestable', 
                ['App\Supply'],
                function(Builder $query) use($req) {
                    $query->where("user_id", Auth::user()->id)
                    ->where("status_id", $req->status_id);
                }
                )->get();
        } else {
            $statuses = Status::all();
            $supplyRequests = \App\Request::whereHasMorph('requestable', ['App\Supply'])->get();
        }
        
        $equipment = Equipment::all();
        if(isset($req->status_id) && $req->status_id>0)
        {
            $equipmentRequests = \App\Request::whereHasMorph(
                'requestable', 
                ['App\Equipment'],
                function(Builder $query) use($req) {
                    $query->where("user_id", Auth::user()->id)
                    ->where("status_id", $req->status_id);
                }
                )->get();
        } else {
            $statuses = Status::all();
            $equipmentRequests = \App\Request::whereHasMorph('requestable', ['App\Equipment'])->get();
        }    
        return view('/assets', compact('supplies', 'supplyRequests', 'equipment', 'equipmentRequests', 'statuses', 'statusId'));
    }
}
