<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApprovalRequest extends Model
{
    protected $fillable = ['approver_id', 'request_id', 'status_id'];

    public function approver()
    {
        return $this->belongsTo('\App\User', 'approver_id', 'id');
    }

    public function request()
    {
        return $this->belongsTo('\App\Request');
    }

    public function status()
    {
        return $this->belongsTo('\App\Status');
    }
}
