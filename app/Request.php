<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Request extends Model
{
    protected $fillable = ['user_id', 'quantity', 'status_id'];

    public function requestable()
    {
        return $this->morphTo();
    }

    public function status()
    {
        return $this->belongsTo('App\Status');
    }

    public function requester()
    {
    	return $this->belongsTo("\App\User", 'user_id', 'id');
    }
    
    public function approvalRequests()
    {
        return $this->hasMany('\App\ApprovalRequest');
    }

}
