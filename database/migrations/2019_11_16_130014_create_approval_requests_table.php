<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApprovalRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('approval_requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('approver_id');
            $table->unsignedBigInteger('request_id');
            $table->unsignedBigInteger('status_id');
            $table->timestamps();

            $table->foreign('approver_id')
                ->on('users')
                ->references('id')
                ->onDelete('restrict')
                ->onUpdate('cascade');

            $table->foreign('request_id')
                ->on('requests')
                ->references('id')
                ->onDelete('restrict')
                ->onUpdate('cascade');

            $table->foreign('status_id')
                ->on('statuses')
                ->references('id')
                ->onDelete('restrict')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('approval_requests');
    }
}
