<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\ItemController;

Route::get('/', function () {
    return view('landing');
});

Route::get('/logout', function () {
    return view('landing');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/assets', 'AssetController@index');
Route::post('/assets', 'AssetController@index');

Route::middleware("user")->group(function () {
    // User Requests
    Route::patch('/requests/{id}', 'RequestController@update');
    Route::post('/requests', 'RequestController@store');
    Route::delete('/deleterequest/{id}', 'RequestController@destroy');
    // User Profile
    Route::get('/profile', 'ProfileController@index');
    Route::patch('/profile', 'ProfileController@update');
    // User Request for Approval
    Route::get('/approvalrequests', 'ApprovalRequestController@index');
});

Route::patch('/supplies/{id}/approve', 'SupplyRequestController@approveRequest');
Route::patch('/supplies/{id}/disapprove', 'SupplyRequestController@disapproveRequest');
Route::patch('/equipment/{id}/approve', 'EquipmentRequestController@approveRequest');
Route::patch('/equipment/{id}/disapprove', 'EquipmentRequestController@disapproveRequest');


Route::middleware("admin")->group(function () {
    // Admin home page
    Route::get('/dashboard', 'DashboardController@index');

    // Supplies
    Route::get('/supplies', 'SupplyController@index');
    Route::get('/addsupply', 'SupplyController@create');
    Route::post('/addsupply', 'SupplyController@store');
    Route::get('/editsupply/{id}', 'SupplyController@edit');
    Route::patch('/supplies/{id}', 'SupplyController@update');
    Route::delete('/deletesupply/{id}', 'SupplyController@destroy');

    // Supplies Requests
    Route::get('/suppliesrequests', 'SupplyRequestController@index');
    Route::patch('/supplies/{id}/claim', 'SupplyRequestController@claimRequest');

    // Equipment
    Route::get('/equipment', 'EquipmentController@index');
    Route::get('/addequipment', 'EquipmentController@create');
    Route::post('/addequipment', 'EquipmentController@store');
    Route::get('/editequipment/{id}', 'EquipmentController@edit');
    Route::patch('/equipment/{id}', 'EquipmentController@update');
    Route::delete('/deleteequipment/{id}', 'EquipmentController@destroy');

    // Equipment Requests
    Route::get('/equipmentrequests', 'EquipmentRequestController@index');
    Route::patch('/equipment/{id}/claim', 'EquipmentRequestController@claimRequest');
});
