(() => {
  $('#btn-submit-edit').click((e) => {
    // Prevents submission of the form.
    e.preventDefault();

    // input fields from the edit supply modal
    const txtEditSupplyName = document.getElementById('text-edit-supply-name');
    const txtEditSupplyDescription = document.getElementById('text-edit-supply-description');
    const txtEditSupplyStock = document.getElementById('text-edit-supply-stock');
    const hidEditSupplyId = document.getElementById('hidden-edit-supply-id');
    const hidFormToken = document.getElementById('edit-supply-form-token');

    const updatedSupply = {
      name: txtEditSupplyName.value,
      description: txtEditSupplyDescription.value,
      stock: txtEditSupplyStock.value
    };

    fetch('/supplies/' + hidEditSupplyId.value, {
      method: 'PATCH',
      body: JSON.stringify(updatedSupply),
      headers: {
        'Content-Type': 'application/json',
        'X-CSRF-Token': hidFormToken.value
      }
    }).then((response) => {
      console.log(response)
      if (response.statusText === 'OK') {
        window.alert('Item successfully updated');
        window.location.reload();
      } else {
        window.alert('There was a problem updating the item');
        window.location.reload();
      }
    }).catch((e) => {
      console.log(e)
    })
  })

  $('.btn-edit-supply').click((e) => {
    const elem = e.currentTarget;

    // supply id from the edit button
    const supplyId = elem.getAttribute('data-id');
    
    // input fields from the edit supply modal
    const txtEditSupplyName = document.getElementById('text-edit-supply-name');
    const txtEditSupplyDescription = document.getElementById('text-edit-supply-description');
    const txtEditSupplyStock = document.getElementById('text-edit-supply-stock');
    const hidEditSupplyId = document.getElementById('hidden-edit-supply-id');

    // values from the td tag
    const tdSupplyName = document.getElementById('td-supply-name-' + supplyId);
    const tdSupplyDescription = document.getElementById('td-supply-description-' + supplyId);
    const tdSupplyStock  = document.getElementById('td-supply-stock-' + supplyId);

    // set the values of the edit supply modal input fields
    txtEditSupplyName.value = tdSupplyName.getAttribute('data-value');
    txtEditSupplyDescription.value = tdSupplyDescription.getAttribute('data-value');
    txtEditSupplyStock.value = tdSupplyStock.getAttribute('data-value');
    hidEditSupplyId.value = supplyId;


    $('#editSupplyModal').modal('show');
  });
})();
