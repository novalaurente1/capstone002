(() => {
  // show the active tab based on the hash's value
  if (window.location.hash === '#equipment') {
    $('#tab-menu-equipment').tab('show');
  } else if (window.location.hash === '#supply') {
    $('#tab-menu-supply').tab('show');
  }

  // Set default hash to supply if no hash is provided
  if (window.location.hash === '') {
    window.location.hash = 'supply';
  }

  document.getElementById('btn-filter-equipment').onclick = e => {
    e.preventDefault();

    const formEquipment = document.getElementById('form-equipment');

    formEquipment.action = formEquipment.action + '#equipment';

    formEquipment.submit();
  };

  document.getElementById('btn-filter-supply').onclick = e => {
    e.preventDefault();

    const formSupply = document.getElementById('form-supply');

    formSupply.action = formSupply.action + '#supply';

    formSupply.submit();
  };

  document.getElementById('tab-menu-supply').onclick = e => {
    window.location.hash = 'supply';
  };

  document.getElementById('tab-menu-equipment').onclick = e => {
    window.location.hash = 'equipment';
  };

  const btnCreateSupplyRequest = document.getElementById('btn-create-supply-request');
  const btnCreateEquipmentRequest = document.getElementById('btn-create-equipment-request');
  const btnUpdateSupplyRequest = document.getElementById('btn-update-supply-request');
  const btnUpdateEquipmentRequest = document.getElementById('btn-update-equipment-request');

  // Button to finally update the supply request from the database.
  btnUpdateSupplyRequest.onclick = e => {
    // input fields from the edit supply modal
    const hidEditSupplyRequestId = document.getElementById('hid-edit-supply-request-id');
    const inpEditSupplyQuantity = document.getElementById('inp-edit-supply-request-quantity');

    const payload = {
      quantity: inpEditSupplyQuantity.value
    };

    fetch('/requests/' + hidEditSupplyRequestId.value, {
      method: 'PATCH',
      body: JSON.stringify(payload),
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(response => {
        if (response.statusText === 'OK') {
          // Successful update. show some success message
          window.alert('Request is successfully updated.');
          window.location.reload();
        } else {
          window.alert('There was a problem updating the request.');
        }
      })
      .catch(e => {
        console.log(e);
      });
  };

  // Button to finally update the equipment request from the database.
  btnUpdateEquipmentRequest.onclick = e => {
    // input fields from the edit equipment modal
    const hidEditEquipmentRequestId = document.getElementById('hid-edit-equipment-request-id');
    const inpEditEquipmentQuantity = document.getElementById('inp-edit-equipment-request-quantity');

    const payload = {
      quantity: inpEditEquipmentQuantity.value
    };

    fetch('/requests/' + hidEditEquipmentRequestId.value, {
      method: 'PATCH',
      body: JSON.stringify(payload),
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(response => {
        if (response.statusText === 'OK') {
          // Successful update. show some success message
          window.alert('Successfully updated the equipment request.');
          window.location.reload();
        } else {
          // there is an error. render some error message
        }
      })
      .catch(e => {
        console.log(e);
      });
  };

  $('.btn-edit-request').click(e => {
    const elem = e.currentTarget;

    // Request that is currently being edited.
    const requestId = elem.getAttribute('data-id');
    const requestType = elem.getAttribute('data-type');

    if (requestType === 'supply') {
      // Data from the table
      const tdSupply = document.getElementById('td-supply-' + requestId);
      const tdQuantity = document.getElementById('td-supply-quantity-' + requestId);

      // Modal input fields
      const selectEditSupply = document.getElementById('sel-edit-supply-request-supply');
      const inpEditSupplyQuantity = document.getElementById('inp-edit-supply-request-quantity');
      const hidEditSupplyRequestId = document.getElementById('hid-edit-supply-request-id');

      // Update the values of the input fields
      selectEditSupply.value = tdSupply.getAttribute('data-value');
      inpEditSupplyQuantity.value = tdQuantity.getAttribute('data-value');
      hidEditSupplyRequestId.value = requestId;

      // Show the moodal.
      $('#modal-edit-supply').modal('show');
    } else if (requestType === 'equipment') {
      // Data from the table
      const tdEquipment = document.getElementById('td-equipment-' + requestId);
      const tdQuantity = document.getElementById('td-equipment-quantity-' + requestId);

      // Modal input fields
      const selectEditEquipment = document.getElementById('sel-edit-equipment-request-equipment');
      const inpEditEquipmentQuantity = document.getElementById(
        'inp-edit-equipment-request-quantity'
      );
      const hidEditEquipmentRequestId = document.getElementById('hid-edit-equipment-request-id');

      // Update the values of the input fields
      selectEditEquipment.value = tdEquipment.getAttribute('data-value');
      inpEditEquipmentQuantity.value = tdQuantity.getAttribute('data-value');
      hidEditEquipmentRequestId.value = requestId;

      // Show the moodal.
      $('#modal-edit-equipment').modal('show');
    }
  });

  btnCreateSupplyRequest.onclick = e => {
    e.preventDefault();

    const inpSupplyQuantity = document.getElementById('inp-create-supply-request-quantity');
    const selSupply = document.getElementById('sel-create-supply-request-supply');
    const hidSupplyToken = document.getElementById('hid-create-supply-request-token');
    const hidSupplyUser = document.getElementById('hid-create-supply-request-user');

    fetch('/requests', {
      method: 'POST',
      body: JSON.stringify({
        request_type: 'supply',
        quantity: inpSupplyQuantity.value,
        user: hidSupplyUser.value,
        supply: selSupply.value
      }),
      headers: {
        'Content-Type': 'application/json',
        'X-CSRF-Token': hidSupplyToken.value
      }
    })
      .then(response => {
        if (response.statusText === 'OK') {
          window.alert('Supply request successfully created.');
          window.location.reload();
        } else {
          window.alert('There was a problem creating a supply request');
          window.location.reload();
        }
      })
      .catch(e => {
        console.log(e);
      });
  };

  btnCreateEquipmentRequest.onclick = e => {
    e.preventDefault();

    const inpEquipmentQuantity = document.getElementById('inp-create-equipment-request-quantity');
    const selEquipment = document.getElementById('sel-create-equipment-request-equipment');
    const hidEquipmentToken = document.getElementById('hid-create-equipment-request-token');
    const hidEquipmentUser = document.getElementById('hid-create-equipment-request-user');

    fetch('/requests', {
      method: 'POST',
      body: JSON.stringify({
        request_type: 'equipment',
        quantity: inpEquipmentQuantity.value,
        user: hidEquipmentUser.value,
        equipment: selEquipment.value
      }),
      headers: {
        'Content-Type': 'application/json',
        'X-CSRF-Token': hidEquipmentToken.value
      }
    })
      .then(response => {
        if (response.statusText === 'OK') {
          window.alert('Equipment request successfully created.');
          window.location.reload();
        } else {
          window.alert('There was a problem creating an equipment request');
          // window.location.reload();
        }
      })
      .catch(e => {
        console.log(e);
      });
  };
})();
