(() => {
  // show the active tab based on the hash's value
  if (window.location.hash === '#equipment') {
    $('#tab-menu-equipment').tab('show');
  } else if (window.location.hash === '#supply') {
    $('#tab-menu-supply').tab('show');
  }

  // Set default hash to supply if no hash is provided
  if (window.location.hash === '') {
    window.location.hash = 'supply';
  }

  document.getElementById('tab-menu-supply').onclick = e => {
    window.location.hash = 'supply';
  };

  document.getElementById('tab-menu-equipment').onclick = e => {
    window.location.hash = 'equipment';
  };

  document.getElementById('tab-menu-supply').onclick = e => {
    window.location.hash = 'supply';
  };

  document.getElementById('tab-menu-equipment').onclick = e => {
    window.location.hash = 'equipment';
  };

  $('.btn-approvalrequest').click((e) => {
    e.preventDefault();

    const elem = e.currentTarget;
    const buttonModule = elem.getAttribute('data-module');

    const approvalRequestForm = $(elem).closest('.form-approvalrequest');

    approvalRequestForm.attr('action', approvalRequestForm.attr('action') + '#' + buttonModule);
    approvalRequestForm.submit();
  });
})();
