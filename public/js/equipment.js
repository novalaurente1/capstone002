(() => {
  $("#btn-submit-edit").click(e => {
    e.preventDefault();

    const txtEditEquipmentName = document.getElementById(
        "text-edit-equipment-name"
    );
    const txtEditEquipmentDescription = document.getElementById(
        "text-edit-equipment-description"
    );
    const txtEditEquipmentStock = document.getElementById(
        "text-edit-equipment-stock"
    );
    const hidEditEquipmentId = document.getElementById(
        "hidden-edit-equipment-id"
    );
    const hidFormToken = document.getElementById(
        "edit-equipment-form-token"
    );

    const updatedEquipment = {
        name: txtEditEquipmentName.value,
        description: txtEditEquipmentDescription.value,
        stock: txtEditEquipmentStock.value
    };

    fetch("/equipment/" + hidEditEquipmentId.value, {
        method: "PATCH",
        body: JSON.stringify(updatedEquipment),
        headers: {
            "Content-Type": "application/json",
            "X-CSRF-Token": hidFormToken.value
        }
    })
        .then(response => {
            console.log(response);
            if (response.statusText === "OK") {
                window.alert("Item successfully updated");
                window.location.reload();
            } else {
                window.alert("There was a problem updating the item");
                window.location.reload();
            }
        })
        .catch(e => {
            console.log(e);
        });
    });

    $(".btn-edit-equipment").click(e => {
        const elem = e.currentTarget;

        const equipmentId = elem.getAttribute("data-id");

        const txtEditEquipmentName = document.getElementById(
            "text-edit-equipment-name"
        );
        const txtEditEquipmentDescription = document.getElementById(
            "text-edit-equipment-description"
        );
        const txtEditEquipmentStock = document.getElementById(
            "text-edit-equipment-stock"
        );
        const hidEditEquipmentId = document.getElementById(
            "hidden-edit-equipment-id"
        );

        const tdEquipmentName = document.getElementById(
            "td-equipment-name-" + equipmentId
        );
        const tdEquipmentDescription = document.getElementById(
            "td-equipment-description-" + equipmentId
        );
        const tdEquipmentStock = document.getElementById(
            "td-equipment-stock-" + equipmentId
        );

        txtEditEquipmentName.value = tdEquipmentName.getAttribute("data-value");
        txtEditEquipmentDescription.value = tdEquipmentDescription.getAttribute(
            "data-value"
        );
        txtEditEquipmentStock.value = tdEquipmentStock.getAttribute(
            "data-value"
        );
        hidEditEquipmentId.value = equipmentId;

        $("#editEquipmentModal").modal("show");
    });
})();
