@extends('templates.admintemplate')
@section('title', 'All Supplies')
@section('content')

<div class="container">
	<h1 class="col-lg-11 py-3">Supplies</h1>
	{{-- ADD SUPPLY modal start --}}
	<div class="col-lg-3 pl-0 ml-4 my-3">
		<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addSupplyModal">
			Add New
		</button>
	</div>

	<div class="modal" id="addSupplyModal">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Add Supply</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body d-flex justify-content-center">
					<div class="col-lg-10">
						<form action="/addsupply" method="POST">
							@csrf
							<div class="form-group">
								<label for="name">Item Name</label>
								<input type="text" name="name" class="form-control">
							</div>
							<div class="form-group">
								<label for="description">Item Description</label>
								<textarea name="description" class="form-control"></textarea>
							</div>
							<div class="form-group">
								<label for="stock">Stock</label>
								<input type="number" name="stock" class="form-control">
							</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Save changes</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
				</form>
			</div>
		</div>
	</div>
	{{-- ADD SUPPLY modal end --}}
	<div class="row">
		<div class="col-lg-11 ml-3">
			<table class="table table-striped border">
				<thead>
					<tr>
						<th>@sortablelink('id')</th>
						<th>@sortablelink('name')</th>
						<th>@sortablelink('description')</th>
						<th>@sortablelink('stock')</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($supplies as $supply)
					<tr>
						<td>00{{$supply->id}}</td>
						<td id="td-supply-name-{{$supply->id}}" data-value="{{$supply->name}}">{{$supply->name}}</td>
						<td id="td-supply-description-{{$supply->id}}" data-value="{{$supply->description}}">{{$supply->description}}</td>
						<td id="td-supply-stock-{{$supply->id}}" data-value="{{$supply->stock}}">{{$supply->stock}}</td>
						<td>
							<div class="d-flex">
							<button type="button" data-id="{{$supply->id}}" class="btn btn-edit-supply btn-success">
									Edit
								</button>
								<form action="/deletesupply/{{$supply->id}}" method="POST">
									@csrf
									@method('DELETE')
									<button class="btn btn-danger ml-3" type="submit">Delete</button>
								</form>
							</div>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			{!! $supplies->appends(\Request::except('page'))->render() !!}
		</div>
	</div>
</div>

{{-- EDIT SUPPLY modal start --}}
<div class="modal" id="editSupplyModal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Edit Supply</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body d-flex justify-content-center">
				<div class="col-lg-10">
				<form method="POST">
						@csrf
						@method('PATCH')
						<div class="form-group">
							<label for="name">Item Name</label>
							<input type="hidden" value="{{ csrf_token() }}" name="token" class="form-control" id="edit-supply-form-token" />
							<input type="hidden" name="edit_supply_id" class="form-control" id="hidden-edit-supply-id" />
							<input type="text" name="name" class="form-control" id="text-edit-supply-name" />
						</div>
						<div class="form-group">
							<label for="description">Item Description</label>
							<textarea name="description" class="form-control" id="text-edit-supply-description"></textarea>
						</div>
						<div class="form-group">
							<label for="stock">Stock</label>
							<input type="number" name="stock" class="form-control" id="text-edit-supply-stock">
						</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" id="btn-submit-edit" class="btn btn-primary">Save changes</button>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
			</form>
		</div>
	</div>
</div>
{{-- EDIT SUPPLY modal start --}}
@endsection

@section('js')
	<script type="text/javascript" src="js/supplies.js"></script>
@endsection