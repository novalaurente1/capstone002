@extends('templates.usertemplate')
@section("title", "Home")

@section('content')
<div class="container">
    <div class="row d-flex justify-content-center">
        <div class="col-md-8 stretch-card grid-margin">
            <div class="card bg-danger text-white">
                <div class="card-body d-flex">
                    <h3 class="font-weight-normal text-white">Pending Requests</h3>
                    <a href="{{(Auth::user()->manager_id !== null && Auth::user()->manager_id !== 0) ? 'assets' : 'approvalrequests'}}" class="text-white">
                        <p class="text-white pl-5" style="font-size: 72px">{{$totalRequests}}</p>
                    </a>
                </div>
            </div>
        </div>
    </div>
    @if(Auth::user()->manager_id !== null && Auth::user()->manager_id !== 0)
        <div class="row d-flex justify-content-center mt-4">
            <div class="col-md-4 stretch-card grid-margin">
                <div class="card bg-success card-img-holder text-white">
                    <div class="card-body mx-auto">
                        <h2 class="font-weight-normal py-4 text-center">
                            <a class="text-white" href="assets">Request for Supply</a>
                        </h2>
                    </div>
                </div>
            </div>

            <div class="col-md-4 stretch-card grid-margin">
                <div class="card bg-primary card-img-holder text-white">
                    <div class="card-body mx-auto">
                        <h2 class="font-weight-normal py-4 text-center">
                            <a class="text-white" href="/assets#equipment">Request for Equipment</a>
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    @endif
</div>
@endsection

@section('js')
@endsection