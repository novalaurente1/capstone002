@extends('templates.admintemplate')
@section("title", "Add Supply")

@section('content')

<h1 class="ml-5 mb-3">Add Supply</h1>
    
<div class="container admin-forms">
    <div class="row">
        <div class="col-lg-6 mb-5">
            <form action="/addsupply" method="POST">
                @csrf
                <div class="form-group">
                    <label for="name">Item Name</label>
                    <input type="text" name="name" class="form-control">
                </div>
                <div class="form-group">
                    <label for="description">Item Description</label>
                    <textarea name="description" class="form-control"></textarea>
                </div>
                <div class="form-group">
                    <label for="stock">Stock</label>
                    <input type="number" name="stock" class="form-control">
                </div>
                <div class="d-flex">
                    <button type="submit" class="btn btn-success ml-auto">Add Item</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
