@extends('templates.admintemplate')
@section('title', 'Supplies Requests')
@section('content')

<div class="container">
	<h1 class="col-lg-11 py-3">Supplies Requests</h1>
    @if(Session::has("message"))
	<div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    	{{ Session::get('message', '') }}
    </div>
	@endif
	<div class="row">
		<div class="col-lg-11 ml-3">
			<table class="table table-striped border">
				<thead>
					<tr>
						<th>Id</th>
						<th>Date Requested</th>
						<th>Employee</th>
						<th>Item</th>
						<th>Quantity</th>
						<th>Stock</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach($supplyRequests as $request)
					<tr>
						<td>{{$request->created_at->format('U')}}-{{$request->id}}</td>
						<td>{{$request->created_at->diffForHumans()}}</td>
						<td>{{$request->requester->name}}</td>
						<td>{{$request->requestable->name}}</td>
                    	<td>{{$request->quantity}}</td>
						<td>{{$request->requestable->stock}}</td>
						<td>{{$request->status->name}}</td>
						<td>
							<div class="d-flex requests-actions">
								@if($request->status->name === 'Pending')
									<form action="/supplies/{{$request->id}}/approve" method="POST">
										@csrf
										@method('PATCH')
										<button type="submit" class="btn btn-success btn-approve-supply-request btn-approval">
											Approve
										</button>
									</form>
									<form action="/supplies/{{$request->id}}/disapprove" method="POST">
										@csrf
										@method('PATCH')
										<button class="btn btn-danger btn-approval btn-disapprove-supply-request" type="submit">Disapprove</button>
									</form>
								@endif
								@if($request->status->name === 'Approved')
									<form action="/supplies/{{$request->id}}/claim" method="POST">
										@csrf
										@method('PATCH')
										<button class="btn btn-primary btn-approval btn-claim-supply-request" type="submit">Mark as Claimed</button>
									</form>
								@endif
							</div>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="css/suppliesrequests.css" />
@endsection