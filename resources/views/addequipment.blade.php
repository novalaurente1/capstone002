@extends('templates.admintemplate')
@section("title", "Add Equipment")

@section('content')


<div class="container" id="addEquipmentPage">
    <h1 class="ml-3 mb-3">Add Equipment</h1>
    <form action="/admin/additem" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-lg-5 ml-3">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" class="form-control">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-5 ml-3">
                <div class="form-group">
                    <label for="name">General Description</label>
                    <textarea name="description" class="form-control"></textarea>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-5 ml-3">
                <div class="form-group">
                    <label for="quantity"">Quantity</label>
                        <input type=" number" name="quantity" class="form-control">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-5 ml-3">
                <fieldset class="form-group">
                    <div class="d-flex">
                        <label for="description">Source</label>
                        <div class="form-check">
                            <label class="form-check-label ml-3">
                                <input type="radio" class="form-check-input" name="optionsRadios" id="optionsRadios1"
                                    value="option1" checked="">
                                New Stock
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label ml-3">
                                <input type="radio" class="form-check-input" name="optionsRadios" id="optionsRadios2"
                                    value="option2">
                                From Inventory
                            </label>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-5 ml-3">
                <fieldset class="form-group">
                    <div class="d-flex">
                        <label for="description">Unit</label>
                        <div class="form-check">
                            <label class="form-check-label ml-3">
                                <input type="radio" class="form-check-input" name="optionsRadios" id="optionsRadios1"
                                    value="option1" checked="">
                                Piece
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label ml-3">
                                <input type="radio" class="form-check-input" name="optionsRadios" id="optionsRadios2"
                                    value="option2">
                                Set
                            </label>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-5 ml-3">
                <form class="form-inline my-2 my-lg-0">
                    <h4>Search for Item</h4>
                    <div class="mx-0 d-flex">
                        <input class="form-control mr-sm-2" type="text" placeholder="Search">
                        <button class="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
                    </div>
                </form>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-11 ml-3 my-3">
                <table class="table table-light table-striped border">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Item</th>
                            <th>Stock</th>
                            <th>Quantity</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-11 ml-3 my-3">
                <h4>Components</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-11 ml-3 my-3">
                <table class="table table-light table-striped border">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Item</th>
                            <th>Quantity</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>


        <div class="row">
            <div class="col-lg-11 ml-3 my-3 d-flex justify-content-end">
                <a href="/equipments" class="btn btn-secondary">Cancel</a>
                <button type="submit" class="btn btn-success ml-3">Add Equipment</button>
            </div>
        </div>

        {{-- <div class="row">
                <div class="d-flex">
                    <div class="col-lg-11 my-3 ml-auto">
                        <button type="submit" class="btn btn-success">Add Equipment</button>
                    </div>
                </div>
            </div> --}}
    </form>

</div>
@endsection