@extends('templates.usertemplate')
@section('title', 'Invento - Profile')
@section('content')

<div class="container mw-100" style="margin-top: -30px; height: 85vh; padding: 0 45px;">
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#profile">Profile</a>
        </li>
    </ul>
    <div id="myTabContent" class="tab-content mt-5">
        <div class="tab-pane fade show active" id="profile">
            <form action="/profile" method="POST" style="width: 350px; margin: auto">
                @csrf
                @method('PATCH')
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" class="form-control" id="name" value="{{Auth::user()->name}}">
                </div>

                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" name="email" class="form-control" id="email" value="{{Auth::user()->email}}">
                </div>

                <div class="form-group d-flex justify-content-center">
                    <button type="submit" class="btn btn-success" style="width: 350px">
                        Save Changes
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection