@extends('templates.admintemplate')
@section('title', 'Equipments Requests')
@section('content')

<div class="container">
	<h1 class="col-lg-11 py-3">Equipments Requests</h1>
	{{-- ADD SUPPLY modal start --}}

	<div class="modal" id="addSupplyModal">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Add Supply</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body d-flex justify-content-center">
					<div class="col-lg-10">
						<form action="/addsupply" method="POST">
							@csrf
							<div class="form-group">
								<label for="name">Item Name</label>
								<input type="text" name="name" class="form-control">
							</div>
							<div class="form-group">
								<label for="description">Item Description</label>
								<textarea name="description" class="form-control"></textarea>
							</div>
							<div class="form-group">
								<label for="stock">Stock</label>
								<input type="number" name="stock" class="form-control">
							</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Save changes</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
				</form>
			</div>
		</div>
	</div>
	{{-- ADD SUPPLY modal end --}}
	<div class="row">
		<div class="col-lg-11 ml-3">
			<table class="table table-striped border">
				<thead>
					<tr>
						<th>Id</th>
						<th>Date Requested</th>
						<th>Employee</th>
						<th>Item</th>
						<th>Quantity</th>
						<th>Stock</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>00</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td>
							<div class="d-flex">
								<button type="button" class="btn btn-success" data-toggle="modal" data-target="#editEquipmentModal">
									Approve
								</button>
								<form action="/deletesupply" method="POST">
									
									<button class="btn btn-danger ml-3" type="submit">Disapprove</button>
								</form>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>

{{-- EDIT SUPPLY modal start --}}
<div class="modal" id="editEquipmentModal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Edit Supply</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body d-flex justify-content-center">
				<div class="col-lg-10">
				<form action="/editsupply" method="POST">
						@csrf
						@method('PATCH')
						<div class="form-group">
							<label for="name">Item Name</label>
							<input type="text" name="name" class="form-control" value="">
						</div>
						<div class="form-group">
							<label for="description">Item Description</label>
							<textarea name="description" class="form-control">}</textarea>
						</div>
						<div class="form-group">
							<label for="stock">Stock</label>
							<input type="number" name="stock" class="form-control">
						</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary">Save changes</button>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
			</form>
		</div>
	</div>
</div>
{{-- EDIT SUPPLY modal start --}}
@endsection