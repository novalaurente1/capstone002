@extends('templates.admintemplate')
@section('title', 'Admin - Dashboard')
@section('content')

<div class="container">
    <div class="row d-flex justify-content-center">
        <div class="col-md-8 stretch-card grid-margin">
            <div class="card bg-danger text-white">
                <div class="card-body d-flex">
                    <h3 class="font-weight-normal text-white">Pending Requests</h3>
                    <p class="text-white pl-5" style="font-size: 16px">Supplies:</p>
                    <a class="text-white" href="suppliesrequests">
                    <p class="text-white pl-5" style="font-size: 36px">{{$totalSuppliesRequests}}</p>
                    </a>
                    <p class="text-white pl-5" style="font-size: 16px">Equipment:</p>
                    <a class="text-white" href="equipmentrequests">
                        <p class="text-white pl-5" style="font-size: 36px">{{$totalEquipmentRequests}}</p>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="row d-flex justify-content-center mt-4">
        <div class="col-md-4 stretch-card grid-margin">
            <div class="card bg-success card-img-holder text-white">
                <div class="card-body mx-auto">
                    <h2 class="font-weight-normal py-4 text-center">
                        <a class="text-white" href="supplies">Add <br> Supply</a>
                    </h2>
                </div>
            </div>
        </div>

        <div class="col-md-4 stretch-card grid-margin">
            <div class="card bg-primary card-img-holder text-white">
                <div class="card-body mx-auto">
                    <h2 class="font-weight-normal py-4 text-center">
                        <a class="text-white" href="equipment">Add Equipment</a>
                    </h2>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection