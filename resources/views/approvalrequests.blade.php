@extends('templates.usertemplate')
@section('title', 'Approval Requests')
@section('content')

<h2 class="col-lg-10 ml-4">Approval Requests</h2>
<div class="container mw-100" style="margin-top: 0px; height: 85vh; padding: 0 45px;">
    <ul class="nav nav-tabs" id="tab-approval-requests">
        <li class="nav-item">
            <a class="nav-link active" id="tab-menu-supply" data-toggle="tab" href="#supplies">Supplies</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" id="tab-menu-equipment" href="#equipment">Equipment</a>
        </li>
    </ul>
    <div id="myTabContent" class="tab-content">
        {{-- SUPPLIES TAB start --}}
        <div class="tab-pane fade show active" id="supplies">
            <div class="row">
                <div class="col-lg-10 offset-1">
                    <div class="row d-flex flex-column">
                        <div class="col-lg-10 ml-3">
                            <table class="table table-striped border mt-5">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Date Requested</th>
                                        <th>Requested By</th>
                                        <th>Item</th>
                                        <th>Quantity</th>
                                        <th>Request Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($supplyRequests as $request)
                                    <tr>
                                        <td>{{$request->id}}</td>
                                        <td>{{$request->created_at->diffForHumans()}}</td>
                                        <td>{{$request->requester->name}}</td>
                                        <td id="td-supply-{{$request->id}}" data-value="{{$request->requestable->id}}">
                                            {{$request->requestable->name}}</td>
                                        <td id="td-supply-quantity-{{$request->id}}"
                                            data-value="{{$request->quantity}}">{{$request->quantity}}</td>
                                        <td>{{$request->status->name}}</td>
                                        <td>
                                            <div class="d-flex">
                                                @if(in_array($request->status->name, ['Pending', 'Disapproved']) && $request->admin_status_name === 'Pending')
                                                    <form action="/supplies/{{$request->id}}/approve" method="POST" class="form-approvalrequest">
                                                        @csrf
                                                        @method('PATCH')
                                                        <button type="submit" class="btn btn-success btn-approvalrequest" data-module="supply">
                                                            Approve
                                                        </button>
                                                    </form>
                                                @endif
                                                @if(in_array($request->status->name, ['Pending', 'Approved']) && $request->admin_status_name === 'Pending')
                                                    <form action="/supplies/{{$request->id}}/disapprove" method="POST" class="form-approvalrequest">
                                                        @csrf
                                                        @method('PATCH')
                                                        <button type="submit" class="btn btn-danger ml-3 btn-approvalrequest" data-module="supply">
                                                            Disapprove
                                                        </button>
                                                    </form>
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- SUPPLIES TAB end --}}

        <div class="tab-pane fade" id="equipment">
            <div class="row">
                <div class="col-lg-10 offset-1">
                    <div class="row d-flex flex-column">
                        <div class="col-lg-10 ml-3">
                            <table class="table table-striped border mt-5">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Date Requested</th>
                                        <th>Requested By</th>
                                        <th>Item</th>
                                        <th>Quantity</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($equipmentRequests as $request)
                                    <tr>
                                        <td>{{$request->id}}</td>
                                        <td>{{$request->created_at->diffForHumans()}}</td>
                                        <td>{{$request->requester->name}}</td>
                                        <td id="td-equipment-{{$request->id}}" data-value="{{$request->requestable->id}}">{{$request->requestable->name}}</td>
                                        <td id="td-equipment-quantity-{{$request->id}}" data-value="{{$request->quantity}}">{{$request->quantity}}</td>
                                        <td>{{$request->status->name}}</td>
                                        <td>
                                            <div class="d-flex">
                                                @if(in_array($request->status->name, ['Pending', 'Disapproved']) && $request->admin_status_name === 'Pending')
                                                    <form action="/equipment/{{$request->id}}/approve" method="POST" class="form-approvalrequest">
                                                        @csrf
                                                        @method('PATCH')
                                                        <button type="submit" class="btn btn-success btn-approvalrequest" data-module="equipment">
                                                            Approve
                                                        </button>
                                                    </form>
                                                @endif
                                                @if(in_array($request->status->name, ['Pending', 'Approved']) && $request->admin_status_name === 'Pending')
                                                    <form action="/equipment/{{$request->id}}/disapprove" method="POST" class="form-approvalrequest">
                                                        @csrf
                                                        @method('PATCH')
                                                        <button type="submit" class="btn btn-danger ml-3 btn-approvalrequest" data-module="equipment">
                                                            Disapprove
                                                        </button>
                                                    </form>
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>


@endsection

@section('js')
    <script type="text/javascript" src="js/approvalrequests.js"></script>
@endsection