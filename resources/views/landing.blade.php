@extends('templates.usertemplate')
@section('title', 'Invento')
@section('content')

<div class="container mw-100" id="homeContainer">

    <div id="homeText" class="row d-flex flex-column">
        <div class="col-lg-6 offset-3">
            <div>
                <h1 class="text-center">Manage your company's assets with ease</h1>
            </div>
            <div>
                <p class="text-center">Invento is the supplies and equipment inventory management system that your modern company needs.</p>
            </div>
            <div class="d-flex justify-content-center">
                <a href="{{ route('login') }}" class="btn btn-outline-success">Login</a>
            </div>
        </div>
    
    </div>
    
</div>

@endsection