<!DOCTYPE html>
<html>

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>@yield('title')</title>

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="css/bootstrap.css">

  <!-- Custom CSS -->
  <link rel="stylesheet" href="css/custom.css">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700|Raleway:400,500,600,700&display=swap"
    rel="stylesheet">

  {{-- Font Awesome --}}
  <script src="https://kit.fontawesome.com/d91fe72af7.js" crossorigin="anonymous"></script>

  @yield('css')
</head>

<body>
  <nav class="navbar navbar-expand-lg navbar-light bg-white">
    <a class="navbar-brand" href="home">
      <span class="logo-text-admin secondary-purple ml-4 mt-3">INVENTO</span>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor03"
      aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarColor03">
      <!-- Authentication Links -->
      @guest
      <!-- Right Side Of Navbar -->
      <ul class="navbar-nav ml-auto">
        {{-- <li class="nav-item">
          <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
        </li> --}}
        @if (Route::has('register'))
        <li class="nav-item">
          <a class="landing-register-button mr-5" href="{{ route('register') }}">{{ __('Register') }}</a>
        </li>
      </ul>
      @endif
      @else
      <!-- Left Side Of Navbar -->
      <ul class="navbar-nav mr-auto">
        @if(Auth::user()->manager_id !== null && Auth::user()->manager_id !== 0)
          <li class="nav-item">
            <a class="nav-link-menu ml-5" href="assets" style="">Assets</a>
          </li>
        @endif
        @if(Auth::user()->manager_id === null || Auth::user()->manager_id === 0)
          <li class="nav-item">
            <a class="nav-link-menu ml-5" href="approvalrequests" style="">Approval Requests</a>
          </li>
        @endif
      </ul>
      <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
          <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"
            aria-haspopup="true" aria-expanded="false" v-pre>
            Hello, {{ Auth::user()->name }} <span class="caret ml-4"></span>
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="profile">
              Profile
            </a>
            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                   document.getElementById('logout-form').submit();">
              {{ __('Logout') }}
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              @csrf
            </form>
          </div>
        </li>
      </ul>
      @endguest
    </div>
  </nav>


  @yield('content')

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
  </script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
    integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
  </script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
    integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
  </script>
  @yield('js')
</body>

</html>