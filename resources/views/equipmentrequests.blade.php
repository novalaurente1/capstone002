@extends('templates.admintemplate')
@section('title', 'Equipment Requests')
@section('content')

<div class="container">
	<h1 class="col-lg-11 py-3">Equipment Requests</h1>
	@if(Session::has("message"))
	<div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    	{{ Session::get('message', '') }}
    </div>
	@endif
	<div class="row">
		<div class="col-lg-11 ml-3">
			<table class="table table-striped border">
				<thead>
					<tr>
						<th>Id</th>
						<th>Date Requested</th>
						<th>Employee</th>
						<th>Item</th>
						<th>Quantity</th>
						<th>Stock</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach($equipmentRequests as $request)
					<tr>
						<td>{{$request->created_at->format('U')}}-{{$request->id}}</td>
						<td>{{$request->created_at->diffForHumans()}}</td>
						<td>{{$request->requester->name}}</td>
						<td>{{$request->requestable->name}}</td>
                    	<td>{{$request->quantity}}</td>
						<td>{{$request->requestable->stock}}</td>
						<td>{{$request->status->name}}</td>
						<td>
							<div class="d-flex">
								@if($request->status->name === 'Pending')
									<form action="/equipment/{{$request->id}}/approve" method="POST">
										@csrf
										@method('PATCH')
										<button type="submit" class="btn btn-success btn-approve-equipment-request">
											Approve
										</button>
									</form>
									<form action="/equipment/{{$request->id}}/disapprove" method="POST">
										@csrf
										@method('PATCH')
										<button class="btn btn-danger ml-3 btn-disapprove-equipment-request" type="submit">Disapprove</button>
									</form>
								@endif
								@if($request->status->name === 'Approved')
									<form action="/equipment/{{$request->id}}/claim" method="POST">
										@csrf
										@method('PATCH')
										<button class="btn btn-primary btn-claim-equipment-request" type="submit">Mark as Claimed</button>
									</form>
								@endif
							</div>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>

@endsection