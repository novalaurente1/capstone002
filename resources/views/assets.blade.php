@extends('templates.usertemplate')
@section('title', 'Assets')
@section('content')

<h2 class="col-lg-10 ml-4">Assets</h2>
<div class="container mw-100" style="margin-top: 0px; height: 85vh; padding: 0 45px;">
    <ul class="nav nav-tabs" id="tab-assets">
        <li class="nav-item">
            <a class="nav-link active" id="tab-menu-supply" data-toggle="tab" href="#supplies">Supplies</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" id="tab-menu-equipment" href="#equipment">Equipment</a>
        </li>
    </ul>
    <div id="myTabContent" class="tab-content">
        {{-- SUPPLIES TAB start --}}
        <div class="tab-pane fade show active" id="supplies">
            <div class="row">
                <div class="col-lg-10 offset-1">
                    <div class="row d-flex flex-column">
                        {{-- REQUEST SUPPLY modal start --}}
                        <div class="col-lg-3 ml-4 my-4 pl-2 pr-0">
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#modal-create-supply">
                                Request for Supply
                            </button>
                        </div>
                        {{-- SUPPLIES REQUEST FILTER start --}}
                        <div class="col-lg-3 ml-4 mb-3 pl-2 pr-0">
                            <form class="px-0 d-flex align-items-center" id="form-supply" action="/assets">
                                @csrf
                                <h6 class="mt-2">Status: </h6>
                                <select name="status_id" class="form-control ml-2" style="width: 125px">
                                    <option value="0">All</option>
                                    @foreach($statuses as $status)
                                    <option value="{{$status->id}}" 
                                        @if(isset($statusId)) 
                                            @if($statusId==$status->id)
                                            selected
                                            @endif
                                        @endif
                                        >{{$status->name}}</option>
                                    @endforeach
                                </select>
                                <button type="submit" class="btn btn-primary ml-2" id="btn-filter-supply">Filter</button>
                            </form>
                        </div>
                        {{-- SUPPLIES REQUEST FILTER end --}}
                        <div class="modal" id="modal-create-supply">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Request for Supply</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body d-flex justify-content-center">
                                        <form>
                                            @csrf
                                            <div class="form-group">
                                                <label for="name">Item</label>
                                                <input type="hidden" value="{{ csrf_token() }}"
                                                    id="hid-create-supply-request-token" />
                                                <input type="hidden" value="{{ Auth::id() }}"
                                                    id="hid-create-supply-request-user" />
                                                <select name="supply_id" class="form-control"
                                                    id="sel-create-supply-request-supply">
                                                    @foreach ($supplies as $supply)
                                                    <option value="{{$supply->id}}">
                                                        {{$supply->name}}
                                                    </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="name">Quantity</label>
                                                <input type="number" name="quantity" class="form-control"
                                                    id="inp-create-supply-request-quantity" />
                                            </div>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-primary"
                                            id="btn-create-supply-request">Save changes</button>
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- REQUEST SUPPLY modal end --}}
                        <div class="col-lg-10 ml-3">
                            <table class="table table-striped border table-supply-requests">
                                <thead>
                                    <tr>
                                        <th>Date Requested</th>
                                        <th>Item</th>
                                        <th>Quantity</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($supplyRequests as $request)
                                    <tr>
                                        <td>{{$request->created_at->diffForHumans()}}</td>
                                        <td id="td-supply-{{$request->id}}" data-value="{{$request->requestable->id}}">
                                            {{$request->requestable->name}}</td>
                                        <td id="td-supply-quantity-{{$request->id}}"
                                            data-value="{{$request->quantity}}">{{$request->quantity}}</td>
                                        <td>{{$request->status->name}}</td>
                                        <td>
                                            <div class="d-flex">
                                                @if($request->status->name === 'Pending')
                                                    <button type="button" class="btn btn-success btn-edit-request" data-type="supply" data-id="{{$request->id}}">
                                                        Edit
                                                    </button>
                                                    <form action="/deleterequest/{{$request->id}}" method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button class="btn btn-danger ml-3" type="submit">Delete</button>
                                                    </form>
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        {{-- EDIT REQUEST SUPPLY modal start --}}
                        <div class="modal" id="modal-edit-supply">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Edit Request for Supply</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body d-flex justify-content-center">
                                        <form>
                                            <div class="form-group">
                                                <label for="name">Item</label>
                                                <input type="hidden" value="{{ csrf_token() }}"
                                                    id="hid-edit-supply-request-token" />
                                                <input type="hidden" id="hid-edit-supply-request-id" />
                                                <select name="supply_id" class="form-control"
                                                    id="sel-edit-supply-request-supply" disabled>
                                                    @foreach ($supplies as $supply)
                                                    <option value="{{$supply->id}}">
                                                        {{$supply->name}}
                                                    </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="name">Quantity</label>
                                                <input type="number" name="quantity" value="1" class="form-control"
                                                    id="inp-edit-supply-request-quantity">
                                            </div>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-primary"
                                            id="btn-update-supply-request">Save Changes</button>
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- EDIT REQUEST SUPPLY modal start --}}
                    </div>
                </div>
            </div>
        </div>
        {{-- SUPPLIES TAB end --}}

        <div class="tab-pane fade 
        {{-- show  --}}
        {{-- active --}}
        " id="equipment">
            <div class="row">
                <div class="col-lg-10 offset-1">
                    <div class="row d-flex flex-column">
                        {{-- REQUEST EQUIPMENT modal start --}}
                        <div class="col-lg-3 pl-0 ml-4 my-4">
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#modal-create-equipment">
                                Request for Equipment
                            </button>
                        </div>
                        {{-- EQUIPMENT REQUEST FILTER start --}}
                        <div class="col-lg-3 ml-4 mb-3 pl-2 pr-0">
                            <form class="px-0 d-flex align-items-center" id="form-equipment" action="/assets" method="GET">
                                @csrf
                                <h6 class="mt-2">Status: </h6>
                                <select name="status_id" class="form-control ml-2" style="width: 125px">
                                    <option value="0">All</option>
                                    @foreach($statuses as $status)
                                        <option value="{{$status->id}}" 
                                            @if(isset($statusId)) 
                                                @if($statusId == $status->id)
                                                selected
                                                @endif
                                            @endif
                                        >{{$status->name}}</option>
                                    @endforeach
                                </select>
                                <button type="submit" class="btn btn-primary ml-2" id="btn-filter-equipment">Filter</button>
                            </form>
                        </div>
                        {{-- EQUIPMENT REQUEST FILTER end --}}
                        <div class="modal" id="modal-create-equipment">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Request for Equipment</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body d-flex justify-content-center">
                                        <form>
                                            @csrf
                                            <div class="form-group">
                                                <label for="name">Item</label>
                                                <input type="hidden" value="{{ csrf_token() }}"
                                                    id="hid-create-equipment-request-token" />
                                                <input type="hidden" value="{{ Auth::id() }}"
                                                    id="hid-create-equipment-request-user" />
                                                <select name="equipment_id" class="form-control"
                                                    id="sel-create-equipment-request-equipment">
                                                    @foreach ($equipment as $item)
                                                    <option value="{{$item->id}}">
                                                        {{$item->name}}
                                                    </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="name">Quantity</label>
                                                <input type="number" name="quantity" value="1" class="form-control"
                                                    id="inp-create-equipment-request-quantity" />
                                            </div>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-primary"
                                            id="btn-create-equipment-request">Save Changes</button>
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- REQUEST SUPPLY modal end --}}
                        <div class="col-lg-10 ml-3">
                            <table class="table table-striped border table-equipment-requests">
                                <thead>
                                    <tr>
                                        <th>Date Requested</th>
                                        <th>Item</th>
                                        <th>Quantity</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($equipmentRequests as $request)
                                    <tr>
                                        <td>{{$request->created_at->diffForHumans()}}</td>
                                        <td id="td-equipment-{{$request->id}}"
                                            data-value="{{$request->requestable->id}}">{{$request->requestable->name}}
                                        </td>
                                        <td id="td-equipment-quantity-{{$request->id}}"
                                            data-value="{{$request->quantity}}">{{$request->quantity}}</td>
                                        <td>{{$request->status->name}}</td>
                                        <td>
                                            <div class="d-flex">
                                                <button type="button" class="btn btn-success btn-edit-request"
                                                    data-type="equipment" data-id="{{$request->id}}">
                                                    Edit
                                                </button>
                                                <form action="/deleterequest/{{$request->id}}" method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button class="btn btn-danger ml-3" type="submit">Delete</button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        {{-- EDIT REQUEST EQUIPMENT modal start --}}
                        <div class="modal" id="modal-edit-equipment">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Edit Request for Supply</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body d-flex justify-content-center">
                                        <form>
                                            @csrf
                                            <div class="form-group">
                                                <label for="name">Item</label>
                                                <input type="hidden" value="{{ csrf_token() }}"
                                                    id="hid-edit-equipment-request-token" />
                                                <input type="hidden" id="hid-edit-equipment-request-id" />
                                                <select name="supply_id" class="form-control"
                                                    id="sel-edit-equipment-request-equipment" disabled>
                                                    @foreach ($equipment as $item)
                                                    <option value="{{$item->id}}">
                                                        {{$item->name}}
                                                    </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="name">Quantity</label>
                                                <input type="number" name="quantity" class="form-control"
                                                    id="inp-edit-equipment-request-quantity" />
                                            </div>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-primary" id="btn-update-equipment-request"
                                            data-type="equipment">Save Changes</button>
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- EDIT REQUEST SUPPLY modal start --}}
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>


@endsection

@section('js')
<script type="text/javascript" src="js/requests.js"></script>
@endsection