@extends('templates.admintemplate')
@section('title', 'All Equipments')
@section('content')

<div class="container">
	<h1 class="col-lg-11 py-3">Equipments</h1>
	{{-- ADD EQUIPMENT modal start --}}
	<div class="col-lg-3 pl-0 ml-4 my-3">
		<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addEquipmentModal">
			Add New
		</button>
	</div>

	<div class="modal" id="addEquipmentModal">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Add Equipment</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body d-flex justify-content-center">
					<div class="col-lg-10">
						<form action="/addequipment" method="POST">
							@csrf
							<div class="form-group">
								<label for="name">Item Name</label>
								<input type="text" name="name" class="form-control">
							</div>
							<div class="form-group">
								<label for="description">Item Description</label>
								<textarea name="description" class="form-control"></textarea>
							</div>
							<div class="form-group">
								<label for="stock">Stock</label>
								<input type="number" name="stock" class="form-control">
							</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Save changes</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
				</form>
			</div>
		</div>
	</div>
	{{-- ADD EQUIPMENT modal end --}}

	<div class="row">
		<div class="col-lg-11 ml-3">
			<table id="all-equipment" class="table table-striped border">
				<thead>
					<tr>
						<th>@sortablelink('id')</th>
						<th>@sortablelink('name')</th>
						<th>@sortablelink('description')</th>
						<th>@sortablelink('stock')</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($equipment as $item)
					<tr>
						<td>00{{$item->id}}</td>
						<td id="td-equipment-name-{{$item->id}}" data-value="{{$item->name}}">{{$item->name}}</td>
						<td id="td-equipment-description-{{$item->id}}" data-value="{{$item->description}}">
							{{$item->description}}</td>
						<td id="td-equipment-stock-{{$item->id}}" data-value="{{$item->stock}}">{{$item->stock}}</td>
						<td>
							<div class="d-flex">
								<button type="button" data-id="{{$item->id}}" class="btn btn-edit-equipment btn-success">
									Edit
								</button>
								<form action="/deleteequipment/{{$item->id}}" method="POST">
									@csrf
									@method('DELETE')
									<button class="btn btn-danger ml-3" type="submit">Delete</button>
								</form>
							</div>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			{!! $equipment->appends(\Request::except('page'))->render() !!}
		</div>
	</div>
</div>
{{-- EDIT EQUIPMENT modal start --}}
<div class="modal" id="editEquipmentModal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Edit Equipment</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body d-flex justify-content-center">
				<div class="col-lg-10">
					<form method="POST">
						@csrf
						@method('PATCH')
						<div class="form-group">
							<label for="name">Item Name</label>
							<input type="hidden" value="{{ csrf_token() }}" name="token" class="form-control"
								id="edit-equipment-form-token" />
							<input type="hidden" name="edit_equipment_id" class="form-control"
								id="hidden-edit-equipment-id" />
							<input type="text" name="name" class="form-control" id="text-edit-equipment-name" />
						</div>
						<div class="form-group">
							<label for="description">Item Description</label>
							<textarea name="description" class="form-control"
								id="text-edit-equipment-description"></textarea>
						</div>
						<div class="form-group">
							<label for="stock">Stock</label>
							<input type="number" name="stock" class="form-control" id="text-edit-equipment-stock">
						</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" id="btn-submit-edit" class="btn btn-primary">Save changes</button>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
			</form>
		</div>
	</div>
</div>
{{-- EDIT EQUIPMENT modal start --}}
@endsection

@section('js')
<script type="text/javascript" src="js/equipment.js"></script>
@endsection